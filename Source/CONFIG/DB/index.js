const mongoose = require("mongoose");
require("dotenv").config();

async function connect() {
  try {
    await mongoose.connect("mongodb://127.0.0.1:27017/Be_Yourself", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      family: 4,
    });
    console.log("Connect Successfully!!!");
  } catch (err) {
    console.log("Connect Failed!!!");
  }
}

module.exports = { connect };
