const moment = require("moment");

module.exports = {
  formatDate: function (timestamp) {
    return moment(timestamp).format("DD/MM/YYYY HH:mm");
  },
};
