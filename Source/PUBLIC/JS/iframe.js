const socket = io("http://localhost:5500", { path: "/socket.io" });
var postComment = document.querySelector(".commentSubmitBtn");
var newComment = document.getElementById("Comment");
var commentArea = document.querySelector(".comment-area");
var universitySlug = "";
var saveUniBtn = document.querySelector(".saveUni");
var infoCollege = document.querySelector(".info-container");
var CollegeName = document.querySelector("h1").innerText;
var slugsArr = [];
var containComment = false;
let currenteditcomment = null;
var elementEditing = [];
var alertInfo = document.querySelector(".alert");
alertInfo.classList.add("hide");
var closealertBtn = document.querySelector(".close-btn");
var introductionText = document.querySelector(".introduction-text");
var collegeName = introductionText.querySelector("h1").textContent;

/*
<div class="reactions-container">
  <div class="reactions">
            <img src="../../../img/others_imgs/wow.svg" alt="Wow">
            <img src="../../../img/others_imgs/sad.svg" alt="Sad">
            <img src="../../../img/others_imgs/angry.svg" alt="Angry">
  </div>
  <div class="amountReactions"><span>3</span></div>
</div>
 */

let currentVisibleContainer = null;
let hoverTimer = null;
const HOVER_DELAY = 1000;

function debounce(func, delay) {
  let timeout;
  return function (...args) {
    clearTimeout(timeout);
    timeout = setTimeout(() => func.apply(this, args), delay);
  };
}

function checkLikeArea() {
  var likeAreas = document.querySelectorAll(".likeArea");
  likeAreas.forEach((likeArea) => {
    likeArea.addEventListener("mouseover", () => {
      clearTimeout(hoverTimer);
      hoverTimer = setTimeout(() => showReactions(likeArea), HOVER_DELAY);
    });
    likeArea.addEventListener("mouseout", () => {
      clearTimeout(hoverTimer);
      let userComment = likeArea.parentElement.parentElement;
      let reactionsContainer = userComment.querySelector(".reaction-container");
      hideReactions(reactionsContainer);
    });
  });
}
const debouncedSaveReactions = debounce(saveReactions, 5000);

function showReactions(likeArea) {
  let userComment = likeArea.parentElement.parentElement;
  let reactionsContainer = userComment.querySelector(".reaction-container");

  if (
    currentVisibleContainer &&
    currentVisibleContainer !== reactionsContainer
  ) {
    hideReactions(currentVisibleContainer);
  }

  currentVisibleContainer = reactionsContainer;

  var reactions = reactionsContainer.querySelectorAll(".reaction");

  reactions.forEach((reaction) => {
    reaction.addEventListener("mouseover", (e) => {
      e.stopPropagation();
      reactionsContainer.style.display = "flex";
      clearTimeout(reactionsContainer.hideTimeout);
    });

    reaction.addEventListener("mouseout", (e) => {
      e.stopPropagation();
      hideReactions(reactionsContainer);
    });

    reaction.addEventListener("click", (e) => {
      e.stopPropagation();
      e.preventDefault();
      var isLogin = parent.document.querySelector("#user-avar");
      var link = isLogin.querySelector('a[href][title="Tạo tài khoản"]');
      if (link) {
        alertInfo.classList.add("show");
        alertInfo.classList.remove("hide");
        alertInfo.classList.add("showAlert");
        setTimeout(function () {
          alertInfo.classList.remove("show");
          alertInfo.classList.add("hide");
        }, 5000);
        reactionsContainer.style.display = "none";
      } else {
        changeReaction(e.target);
        debouncedSaveReactions(e.target);
        reactionsContainer.style.display = "none";
        reactionsContainer.classList.remove("showing");
      }
    });
  });

  reactionsContainer.style.display = "flex";
  reactionsContainer.classList.add("showing");
}

function saveReactions(reaction) {
  let reactionContainer = reaction.parentElement.parentElement.parentElement;
  let commentId = reactionContainer.getAttribute("data-comment-id");
  let dataServer = [
    {
      reaction: reaction.src,
      place: collegeName,
      universityId: universitySlug,
      commentId: commentId,
    },
  ];
  fetch("/save/reactions", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(dataServer),
  })
    .then((res) => res.json())
    .then((responseData) => {
      console.log(responseData);
      checkReactionStatus(responseData);
    })
    .catch((err) => console.log(err.message));
}

function checkReactionStatus(status) {
  console.log(status);
  if (status === "Success") {
    getReactions();
  } else {
    console.log("Chưa thể bày tỏ cảm xúc");
  }
}

function getReactions() {
  fetch("/save/reactions", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);
    })
    .catch((err) => console.log(err.message));
}

function changeReaction(reaction) {
  let userComment = reaction.parentElement.parentElement.parentElement;
  let actionBar = userComment.querySelector(".actionBar");
  let likeArea = actionBar.querySelector(".likeArea");
  let likeContent = likeArea.querySelector(".likeBtn");
  likeContent.textContent = `Đã ${reaction.alt}`;
  likeContent.style.color = `${reaction.getAttribute("data-color")}`;
}

function hideReactions(reactionsContainer) {
  clearTimeout(reactionsContainer.hideTimeout);
  reactionsContainer.hideTimeout = setTimeout(() => {
    reactionsContainer.style.display = "none";
    reactionsContainer.classList.remove("showing");
    if (currentVisibleContainer === reactionsContainer) {
      currentVisibleContainer = null;
    }
  }, 1000);
}

closealertBtn.addEventListener("click", function () {
  console.log("hi");
  alertInfo.classList.remove("show");
  alertInfo.classList.add("hide");
});

function userComments(userComments) {
  userComments.forEach((userComment) => {
    userComment.addEventListener("mouseover", (e) =>
      showOptions(e.currentTarget)
    );
    userComment.addEventListener("mouseout", (e) =>
      hideOptions(e.currentTarget)
    );
  });
}

function showOptions(userComment) {
  var dataContainer = document.getElementById("data-container");
  var userIdData = dataContainer.getAttribute("data-userId");
  var userCommentId = userComment.getAttribute("data-user-id");
  var option = userComment.querySelector(".options");
  if (option) {
    if (userCommentId === userIdData) {
      option.style.display = "flex";
    }
  }
}

function hideOptions(userComment) {
  var option = userComment.querySelector(".options");
  if (option) {
    option.style.display = "none";
  }
}

moment.updateLocale("vi", {
  relativeTime: {
    future: "trong %s",
    past: "%s trước",
    s: "vài giây",
    ss: "%d giây",
    m: "một phút",
    mm: "%d phút",
    h: "một giờ",
    hh: "%d giờ",
    d: "một ngày",
    dd: "%d ngày",
    M: "một tháng",
    MM: "%d tháng",
    y: "một năm",
    yy: "%d năm",
  },
});

saveUniBtn.addEventListener("click", function (e) {
  e.preventDefault();
  var isLogin = parent.document.querySelector("#user-avar");
  var link = isLogin.querySelector('a[href][title="Tạo tài khoản"]');
  if (link) {
    alertInfo.classList.add("show");
    alertInfo.classList.remove("hide");
    alertInfo.classList.add("showAlert");
    setTimeout(function () {
      alertInfo.classList.remove("show");
      alertInfo.classList.add("hide");
    }, 5000);
  } else {
    changeUniState();
  }
});

function changeUniState() {
  if (
    saveUniBtn.innerHTML ===
    `
  <i class="fa-solid fa-bookmark icon-save"></i>
  `
  ) {
    deleteUni(CollegeName);
    saveUniBtn.innerHTML = `
    <i
    class="fa-regular fa-bookmark icon-save"
  ></i>
    `;
  } else {
    saveUni(CollegeName);
    saveUniBtn.innerHTML = `
  <i class="fa-solid fa-bookmark icon-save"></i>
  `;
  }
}

function deleteUni(university) {
  fetch("/delete", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ university }),
  })
    .then((res) => res.json())
    .then((data) => {
      console.log("Success:", data);
    })
    .catch((err) => console.log(err.message));
}

function saveUni(university) {
  fetch("/save", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ university }),
  })
    .then((res) => res.json())
    .then((data) => {
      console.log("Success:", data);
    })
    .catch((err) => console.log(err.message));
}
postComment.addEventListener("click", function (e) {
  e.preventDefault();
  var isLogin = parent.document.querySelector("#user-avar");
  var link = isLogin.querySelector('a[href][title="Tạo tài khoản"]');
  if (link) {
    newComment.value = "";
    alertInfo.classList.add("show");
    alertInfo.classList.remove("hide");
    alertInfo.classList.add("showAlert");
    setTimeout(function () {
      alertInfo.classList.remove("show");
      alertInfo.classList.add("hide");
    }, 5000);
  } else {
    var comment = newComment.value;
    socket.emit("newComment", { comment, universitySlug, collegeName });
    newComment.value = "";
  }
});

function updateTimeAgo(element, timestamp) {
  element.textContent = moment(timestamp).locale("vi").fromNow();
}

socket.on("comment", (comment) => {
  var demo = [];
  const timeAgo = moment(comment.timestamp).fromNow();
  var userComment = document.createElement("div");
  userComment.classList.add("userComment");
  userComment.dataset.userId = comment.userId;
  userComment.dataset.commentId = comment.commentId;
  var commentContent = `
     <div class="userInfo">
      <a href="/user/@${comment.user}" target="_parent">
      <img src="${comment.img}" alt="User Img" />
    </a>
    <p class="userName">${comment.user}</p>
    </div>
    <p class="userContent">${comment.comment}</p>
    <div class="reaction-container">
      <div class="reaction">
        <img src="../../../img/others_imgs/like.svg" alt="Thích" data-color="#0866FF">
      </div>
      <div class="reaction">
        <img src="../../../img/others_imgs/love.svg" alt="Yêu thích"
        data-color="#F33E58">
      </div>
      <div class="reaction">
        <img src="../../../img/others_imgs/haha.svg" alt="Haha"
        data-color="#F7B228">
      </div>
      <div class="reaction">
        <img src="../../../img/others_imgs/wow.svg" alt="Wow"
        data-color="#F7B32A">
      </div>
      <div class="reaction">
        <img src="../../../img/others_imgs/sad.svg" alt="Buồn"
        data-color="#F9C357">
      </div>
      <div class="reaction">
        <img src="../../../img/others_imgs/angry.svg" alt="Phẫn nộ"
        data-color="#EA7719">
      </div>
    </div>
    <div class="actionBar">
        <div class="likeArea">
          <button class="likeBtn">Thích</button>
        </div>
        <div class="replyArea">
          <button class="replyBtn">Phản hồi</button>
        </div>
    </div>
    <div class="options">
      <button class="btnedit" onclick="editcomment(this)" title="Chỉnh sửa bình luận"><img src="/img/tool_imgs/edit.png" style="width: 100%; height:100%"></button>
      <button class="btndelete" onclick="deletecomment(this)" title="Xóa bình luận"><img src="/img/tool_imgs/delete.png" style="width: 100%; height:100%"></button>
    </div>
  `;
  var timeElement = document.createElement("p");
  timeElement.classList.add("time");
  timeElement.textContent = timeAgo;
  userComment.innerHTML = commentContent;
  userComment.querySelector(".userInfo").appendChild(timeElement);
  demo.push(userComment);
  commentArea.insertBefore(userComment, commentArea.firstChild);
  updateTimeAgo(timeElement, comment.timestamp);
  setInterval(() => {
    updateTimeAgo(timeElement, comment.timestamp);
  }, 1000);
  containComment = true;
  checkComments(containComment, demo, "post");
});

socket.on("deletedcomment", async (data) => {
  const { commentId, userId, userComment } = data;
  var userComments = document.querySelectorAll(".userComment");
  userComments.forEach((usercomment) => {
    var userCommentId = usercomment.getAttribute("data-comment-id");
    var userIdClient = usercomment.getAttribute("data-user-id");
    var userCommentMsg = usercomment.querySelector(".userContent").textContent;
    if (
      userCommentId === commentId &&
      userIdClient === userId &&
      userCommentMsg === userComment
    ) {
      usercomment.remove();
    }
  });
});

socket.on("edittedcomment", async (data) => {
  const { editvalue, Status } = data;
  var currenteditcomment = elementEditing[0];
  const editdiv = currenteditcomment.querySelector(".editdiv");
  currenteditcomment.querySelector(".editextarea").value = "";
  currenteditcomment.removeChild(editdiv);
  currenteditcomment.querySelector(".userContent").innerHTML = editvalue;
  var userInfo = currenteditcomment.querySelector(".userInfo");
  var checkIEle = currenteditcomment.querySelector(".edittedState");
  if (checkIEle) {
  } else {
    var iEle = document.createElement("i");
    iEle.classList.add("edittedState");
    iEle.textContent = Status;
    userInfo.appendChild(iEle);
  }
  elementEditing = [];
});

socket.on("loadComments", (comments) => {
  var demo = [];
  commentArea.innerHTML = "";
  comments.forEach((comment) => {
    const timeAgo = moment(comment.timestamp).fromNow();
    var userComment = document.createElement("div");
    userComment.classList.add("userComment");
    userComment.dataset.userId = comment.userId;
    userComment.dataset.commentId = comment._id;
    var commentContent = `
    <div class="userInfo">
    <a href="/user/@${comment.user}" target="_parent">
      <img src="${comment.img}" alt="User Img" />
    </a>
      <p class="userName">${comment.user}</p>
    </div>
    <p class="userContent">${comment.message}</p>
    <div class="reaction-container">
      <div class="reaction">
        <img src="../../../img/others_imgs/like.svg" alt="Thích" data-color="#0866FF">
      </div>
      <div class="reaction">
        <img src="../../../img/others_imgs/love.svg" alt="Yêu thích"
        data-color="#F33E58">
      </div>
      <div class="reaction">
        <img src="../../../img/others_imgs/haha.svg" alt="Haha"
        data-color="#F7B228">
      </div>
      <div class="reaction">
        <img src="../../../img/others_imgs/wow.svg" alt="Wow"
        data-color="#F7B32A">
      </div>
      <div class="reaction">
        <img src="../../../img/others_imgs/sad.svg" alt="Buồn"
        data-color="#F9C357">
      </div>
      <div class="reaction">
        <img src="../../../img/others_imgs/angry.svg" alt="Phẫn nộ"
        data-color="#EA7719">
      </div>
    </div>
    <div class="actionBar">
        <div class="likeArea">
          <button class="likeBtn">Thích</button>
        </div>
        <div class="replyArea">
          <button class="replyBtn">Phản hồi</button>
        </div>
    </div>
    <div class="options">
      <button class="btnedit" onclick="editcomment(this)" title="Chỉnh sửa bình luận"><img src="/img/tool_imgs/edit.png" style="width: 100%; height:100%"></button>
      <button class="btndelete" onclick="deletecomment(this)" title="Xóa bình luận"><img src="/img/tool_imgs/delete.png" style="width: 100%; height:100%"></button>
    </div>
  `;
    var timeElement = document.createElement("p");
    timeElement.classList.add("time");
    timeElement.textContent = timeAgo;
    userComment.innerHTML = commentContent;
    userComment.querySelector(".userInfo").appendChild(timeElement);
    if (comment.state) {
      var iEle = document.createElement("i");
      iEle.classList.add("edittedState");
      iEle.textContent = comment.state;
      var userInfo = userComment.querySelector(".userInfo");
      userInfo.appendChild(iEle);
    }
    demo.push(userComment);
    updateTimeAgo(timeElement, comment.timestamp);
    setInterval(() => {
      updateTimeAgo(timeElement, comment.timestamp);
    }, 1000);
  });
  if (comments.length !== 0) {
    containComment = true;
    checkComments(containComment, demo, "load");
  } else {
    containComment = false;
    checkComments(containComment, null, "load");
  }
  checkLikeArea();
});
function editcomment(buttonedit) {
  currenteditcomment = buttonedit.parentElement.parentElement;
  elementEditing.push(currenteditcomment);
  if (currenteditcomment.querySelector(".editextarea")) {
  } else {
    const contentcomment =
      currenteditcomment.querySelector(".userContent").textContent;
    const editdiv = document.createElement("div");
    editdiv.classList.add("editdiv");
    editdiv.innerHTML = `<textarea class="editextarea"></textarea>
    <div class="controlcomment">
    <button class="btn-savecomment" title="Lưu chỉnh sửa bình luận"><img src="/img/tool_imgs/check-green.png"></button>
    <button class="btn-canceledit" title="Thoát khỏi chế độ chỉnh sửa bình luận"><img src="/img/tool_imgs/turn-back-red.png"></button>
    </div>`;
    currenteditcomment.appendChild(editdiv);
    currenteditcomment.querySelector(".editextarea").value = contentcomment;
  }
  document.querySelectorAll(".btn-canceledit").forEach((item) => {
    item.addEventListener("click", function () {
      const editdiv = currenteditcomment.querySelector(".editdiv");
      currenteditcomment.removeChild(editdiv);
      currenteditcomment.querySelector(".editextarea").value = "";
    });
  });
  document.querySelectorAll(".btn-savecomment").forEach((item) => {
    item.addEventListener("click", function () {
      const editvalue = currenteditcomment.querySelector(".editextarea").value;
      const currentvalue =
        currenteditcomment.querySelector(".userContent").textContent;
      socket.emit("editComment", { editvalue, currentvalue, universitySlug });
    });
  });
}

function deletecomment(buttondelete) {
  var currentdeletecomment = buttondelete.parentElement.parentElement;
  var commentId = currentdeletecomment.getAttribute("data-comment-id");
  var userId = currentdeletecomment.getAttribute("data-user-id");
  var userComment =
    currentdeletecomment.querySelector(".userContent").textContent;
  socket.emit("deleteComment", {
    commentId,
    userId,
    userComment,
    universitySlug,
  });
}

function checkComments(state, userComment, content) {
  if (state === false && userComment === null) {
    commentArea.style.height = "400px";
    commentArea.innerHTML = `
     <div class="noCommentContainer">
      <img src="../../../img/others_imgs/noComment.png" alt="" class="noCommentImg">
      <p>Chưa có bình luận nào. Hãy là người đầu tiên bình luận</p>
     </div>
    `;
  } else if (state === true && userComment !== null && content === "load") {
    commentArea.style.height = "auto";
    commentArea.innerHTML = "";
    if (commentArea.innerHTML === "") {
      userComment.forEach((usercomment) => {
        commentArea.appendChild(usercomment);
      });
    }
  } else if (state === true && userComment !== null && content === "post") {
    commentArea.style.height = "auto";
    var nocommentUser = commentArea.querySelector(".noCommentContainer");
    if (nocommentUser) {
      commentArea.innerHTML = "";
      userComment.forEach((usercomment) => {
        commentArea.appendChild(usercomment);
      });
    } else {
      if (commentArea.innerHTML === "") {
        userComment.forEach((usercomment) => {
          commentArea.appendChild(usercomment);
        });
      }
    }
  }
  userComments(document.querySelectorAll(".userComment"));
}

window.addEventListener("message", async function (event) {
  var message = event.data;
  var themeLink = document.getElementById("theme-link");
  universitySlug = message.univerSlug;
  if (message.action === "light") {
    themeLink.href = "/universitydetail-light.css";
  } else if (message.action === "dark") {
    themeLink.href = "/universitydetail-dark.css";
  }
  await getCollegeData(message.univerSlug);
  socket.emit("joinUniversity", message.univerSlug);
});

async function getCollegeData(slug) {
  fetch("/history/university")
    .then((res) => res.json())
    .then((data) => showCollegeData(data, slug))
    .catch((err) => console.log(err.message));
}

function showCollegeData(data, slug) {
  var universityData = data;
  var universitiesObj = universityData[0];
  var universitiesArr = universitiesObj.universities;
  universitiesArr.forEach((university) => {
    slugsArr.push(university.slug);
    slugsArr.sort();
  });
  checkSaveUni(slugsArr, slug);
}

function checkSaveUni(arr, slugData) {
  var checkState = arr.find((slug) => {
    return slug === slugData;
  });
  if (checkState) {
    saveUniBtn.innerHTML = `
  <i class="fa-solid fa-bookmark icon-save"></i>
  `;
  } else {
  }
}
document.querySelectorAll(".commentcontai").forEach((e) => {
  e.style.display = "none";
});

document.querySelectorAll(".commenttile").forEach((item) => {
  item.addEventListener("click", function () {
    const comment = item.nextElementSibling;
    const arrow = item.querySelector(".dropdowncommentimg");
    arrow.style.transform = "rotate(0)";
    if (comment.style.display == "block") {
      comment.style.display = "none";
      arrow.style.transform = "rotate(0)";
    } else {
      comment.style.display = "block";
      arrow.style.transform = "rotate(-180deg)";
    }
  });
});
