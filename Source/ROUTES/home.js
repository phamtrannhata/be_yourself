// Khởi tạo các biến sử dụng trong hàm
const express = require("express");
const router = express.Router();
const homeController = require("../APP/CONTROLLERS/HomeController");

// Bắt path "/" để sử lý
router.use("/", homeController.home);

module.exports = router;
