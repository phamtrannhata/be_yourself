// Khởi tạo các biến sử dụng trong file
const homeRouter = require("./home");
const testRouter = require("./test");
const universityController = require("../APP/CONTROLLERS/UniversityController");
const loginController = require("../APP/CONTROLLERS/LoginController");
const userController = require("../APP/CONTROLLERS/UserController");
const registerController = require("../APP/CONTROLLERS/RegisterController");
const account = require("../APP/MIDDLEWARE/Account");
const avatar = require("../APP/MIDDLEWARE/Avatar");
const checkRoute = require("../APP/MIDDLEWARE/Route");
const validation = require("../APP/MIDDLEWARE/Validation");
const passport = require("passport");

// Hàm bắt các path và xử lý
function route(app) {
  // Các phương thức Get
  app.get("/save/reactions", account.isLogin, userController.getReactions);
  app.get("/user/@:username", account.isLogin, userController.getUserData);
  app.get(
    "/save/pomosequence",
    account.isLogin,
    userController.getPomoSequence
  );
  app.get("/save/notesdata", account.isLogin, userController.getNotesData);
  app.get("/save/timetable", account.isLogin, userController.getTimetable);
  app.get("/save/pomotime", account.isLogin, userController.getPomoTime);
  app.get("/save/eventlist", account.isLogin, userController.getEvent);
  app.get("/history/:slug", account.isLogin, checkRoute.getCollege);
  app.get(
    "/save/pomodoroBackground",
    account.isLogin,
    userController.getPomoBg
  );
  app.get(
    "/createnewpass/:slug",
    checkRoute.checkRoute,
    loginController.createnewpassword
  );
  app.get("/logout", account.isLogOut, loginController.logout);
  app.get("/createaccount", account.isLogOut, registerController.registerForm);
  app.get("/login/forgetpassword", loginController.forgotpassword);
  app.get("/university", account.isLogin, universityController.university);
  app.get("/university/:slug", account.isLogin, universityController.show);
  app.get("/test", account.isLogin, testRouter);
  app.get("/story", account.isLogin, homeRouter);
  app.get("/", account.isLogin, homeRouter);
  app.get(
    "/login/fb",
    passport.authenticate("facebook", { scope: ["email", "user_photos"] })
  );
  app.get(
    "/login/fb/cb",
    passport.authenticate("facebook", {
      failureRedirect: "/login",
      successRedirect: "/",
    })
  );
  app.get(
    "/login/gg",
    passport.authenticate("google", { scope: ["profile", "email"] })
  );
  app.get(
    "/login/gg/cb",
    passport.authenticate("google", {
      failureRedirect: "/login",
      successRedirect: "/",
    })
  );

  // Các phương thức Post
  app.post("/save/reactions", account.isLogin, userController.saveReactions);
  app.post(
    "/save/pomosequence",
    account.isLogin,
    userController.savePomoSequence
  );
  app.post("/save/notesdata", account.isLogin, userController.saveNotesData);
  app.post("/save/timetable", account.isLogin, userController.saveTimetable);
  app.post("/save/pomotime", account.isLogin, userController.savePomoTime);
  app.post("/save/eventlist", account.isLogin, userController.saveEvent);
  app.get("/save/todolist", account.isLogin, userController.getTodo);
  app.post("/save/todolist", account.isLogin, userController.saveTodo);
  app.post("/delete", account.isLogin, userController.deleteUni);
  app.post("/save", account.isLogin, userController.saveUni);
  app.post(
    "/save/pomodoroBackground",
    account.isLogin,
    userController.savePomoBg
  );
  app.post("/result/holland", account.isLogin, userController.testResult);
  app.post("/result/mbti", account.isLogin, userController.testResult);
  app.post(
    "/createnewpass/:slug",
    validation.validateForgotpass,
    validation.handleValidationErrorsForgotPass,
    loginController.savenewpassword
  );
  app.post("/uploadavatar", avatar.checkAvatar, userController.avatar);
  app.post("/feedback", loginController.postfeedback);
  app.post("/login/forgetpassword", loginController.resetpassword);
  app.post("/login", loginController.login);
  app.post(
    "/register",
    validation.validateRegistration,
    validation.handleValidationErrors,
    registerController.register
  );
  app.post(
    "/changepass",
    validation.validateChangepass,
    validation.handleValidationErrorsChangePass,
    loginController.changepassword
  );
  app.post("/", account.isLogin, homeRouter);
}

module.exports = route;
