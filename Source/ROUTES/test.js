// Khởi tạo các biến sử dụng trong file
const express = require("express");
const router = express.Router();
const testController = require("../APP/CONTROLLERS/TestController");

// Bắt path "/" để xử lý
router.use("/", testController.test);

module.exports = router;
