// Khai báo các biến sử dụng trong file
const express = require("express");
const { engine } = require("express-handlebars");
const path = require("path");
const port = 5500;
const session = require("express-session");
const accountfbdb = require("./APP/MODELS/Accountfb");
const accountggdb = require("./APP/MODELS/Accountgg");
const passport = require("passport");
const passportgg = require("passport-google-oauth2").Strategy;
const passportfb = require("passport-facebook").Strategy;
const route = require("./ROUTES");
const db = require("./CONFIG/DB");
const flash = require("connect-flash");
const toastr = require("express-toastr");
require("dotenv").config();
const { createServer } = require("node:http");
const { Server } = require("socket.io");
const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
  path: "/socket.io",
});
const helpers = require("./Helpers/helper");
const Account = require("../Source/APP/MODELS/Accountbs");
const Accountgg = require("../Source/APP/MODELS/Accountgg");
const Accountfb = require("../Source/APP/MODELS/Accountfb");
const Comment = require("../Source/APP/MODELS/Comment");

// Kết nối và xử lý các yêu cầu socket từ client
io.on("connection", async (socket) => {
  // Bắt sự kiện xóa comment từ client
  socket.on("deleteComment", async (data) => {
    const { commentId, userId, userComment, universitySlug } = data;

    let checkUser;
    if (socket.request.session.username) {
      checkUser = await Account.findOne({
        name: socket.request.session.username,
      });
    } else if (socket.request.user.name) {
      switch (socket.request.user.provider) {
        case "google":
          checkUser = await Accountgg.findOne({
            name: socket.request.user.name,
          });
          break;
        case "facebook":
          checkUser = await Accountfb.findOne({
            name: socket.request.user.name,
          });
          break;
      }
    }

    if (checkUser) {
      const checkComment = await Comment.findOne({
        universityId: universitySlug,
      });

      if (checkComment) {
        var newCheckComment = checkComment.comment.filter((comment) => {
          return !(
            comment.userId.toString() === userId &&
            comment._id.toString() === commentId &&
            comment.message === userComment
          );
        });
        checkComment.comment = newCheckComment;
        await checkComment.save();
        io.to(universitySlug).emit("deletedcomment", {
          commentId,
          userId,
          userComment,
        });
      }
    }
  });

  // Bắt sự kiện chỉnh sửa comment từ client
  socket.on("editComment", async (data) => {
    const { editvalue, currentvalue, universitySlug } = data;
    let checkUser;
    if (socket.request.session.username) {
      checkUser = await Account.findOne({
        name: socket.request.session.username,
      });
    } else if (socket.request.user.name) {
      switch (socket.request.user.provider) {
        case "google":
          checkUser = await Accountgg.findOne({
            name: socket.request.user.name,
          });
          break;
        case "facebook":
          checkUser = await Accountfb.findOne({
            name: socket.request.user.name,
          });
          break;
      }
    }

    if (checkUser) {
      const checkComment = await Comment.findOne({
        universityId: universitySlug,
      });

      if (checkComment) {
        for (let comment of checkComment.comment) {
          if (
            comment.user === checkUser.name &&
            currentvalue === comment.message
          ) {
            comment.message = editvalue;
            comment.state = "Đã chỉnh sửa";
            await checkComment.save();
            io.to(universitySlug).emit("edittedcomment", {
              editvalue,
              Status: comment.state,
            });
            break;
          }
        }
      }
    }
  });

  // Bắt sự kiện tham gia vào một đại học nào đó từ client
  socket.on("joinUniversity", (universityId) => {
    socket.join(universityId);
    Comment.find({ universityId }).then((comments) => {
      if (comments.length !== 0) {
        var commentsUser = comments[0].comment;
        commentsUser.sort(
          (firstComment, secondComment) =>
            new Date(secondComment.timestamp) - new Date(firstComment.timestamp)
        );

        Promise.all(
          commentsUser.map((comment) => {
            return Promise.any([
              Account.findOne({ name: comment.user }),
              Accountgg.findOne({ name: comment.user }),
              Accountfb.findOne({ name: comment.user }),
            ]).then((user) => {
              if (user && user.img !== comment.img) {
                comment.img = user.img;
              }
              return comment;
            });
          })
        )
          .then((updatedComments) => {
            comments[0].comment = updatedComments;
            return comments[0].save();
          })
          .then(() => {
            io.emit("loadComments", comments[0].comment);
          })
          .catch((error) => {
            console.log("Error updating comments:", error);
          });
      } else {
        io.emit("loadComments", []);
      }
    });
  });

  // Bắt sự kiện post comment từ client và xử lý
  socket.on("newComment", async (data) => {
    const { comment, universitySlug, collegeName } = data;
    console.log(collegeName);

    try {
      let checkUser;
      if (socket.request.session.username) {
        checkUser = await Account.findOne({
          name: socket.request.session.username,
        });
      } else if (socket.request.user && socket.request.user.name) {
        switch (socket.request.user.provider) {
          case "google":
            checkUser = await Accountgg.findOne({
              name: socket.request.user.name,
            });
            break;
          case "facebook":
            checkUser = await Accountfb.findOne({
              name: socket.request.user.name,
            });
            break;
        }
      }

      if (checkUser) {
        const user = checkUser.name;
        const userImg = checkUser.img;

        const newComment = {
          user: user,
          img: userImg,
          message: comment,
          timestamp: new Date(),
          userId: checkUser._id,
          place: collegeName,
        };

        let savedComment;
        const room = await Comment.findOne({ universityId: universitySlug });

        if (room) {
          room.comment.push(newComment);
          await room.save();
          savedComment = room.comment[room.comment.length - 1];
        } else {
          const newRoom = new Comment({
            universityId: universitySlug,
            comment: [newComment],
          });
          await newRoom.save();
          savedComment = newRoom.comment[0];
        }

        io.to(universitySlug).emit("comment", {
          comment: savedComment.message,
          user: savedComment.user,
          timestamp: savedComment.timestamp,
          img: savedComment.img,
          userId: savedComment.userId,
          commentId: savedComment._id,
        });
      }
    } catch (error) {
      console.error("Error handling new comment:", error);
    }
  });
});

// Rút ngắn đuôi file của các file handlebars thành hbs
// Đặc layout mặc định là main
// Sử dụng helpers cho toàn hệ thống
app.engine(
  "hbs",
  engine({
    extname: ".hbs",
    defaultLayout: "main",
    helpers: helpers,
  })
);

//Các cài đặt mặc định của WebServer
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "Resource/VIEWS"));
app.use("/img", express.static("img"));
app.use("/sound", express.static("sound"));
app.use(express.static(path.join(__dirname, "PUBLIC/CSS/")));
app.use(express.static(path.join(__dirname, "PUBLIC/JS/")));
app.use(express.json({ limit: "1000mb" }));
app.use(express.urlencoded({ limit: "1000mb", extended: true }));

// Xử lý và kết nối session tới passport, socket
const sessionMiddleware = session({
  secret: "accountsessionsecret",
  resave: false,
  saveUninitialized: true,
});
app.use(sessionMiddleware);
io.engine.use(sessionMiddleware);
app.use(passport.initialize());
app.use(passport.session());
io.use((socket, next) => {
  passport.authenticate("session", { session: false })(
    socket.request,
    {},
    next
  );
});

// Thiết lập toastr message cho cả WebServer
app.use(flash());
app.use(toastr());
app.use(function (req, res, next) {
  req.toastr.render();
  next();
});

// Khởi tạo passport cho tài khoản gg
passport.use(
  new passportgg(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: "http://localhost:5500/login/gg/cb",
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const user = await accountggdb.findOne({ name: profile._json.name });
        if (user) {
          return done(null, user);
        }
        const newUser = new accountggdb({
          id: profile._json.sub,
          name: profile._json.name,
          email: profile._json.email,
          img: profile.picture,
          provider: profile.provider,
          date_created: new Date(),
        });
        await newUser.save();
        return done(null, newUser);
      } catch (err) {
        return done(err);
      }
    }
  )
);

// Khởi tạo passport cho tài khoản fb
passport.use(
  new passportfb(
    {
      clientID: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
      callbackURL: "http://localhost:5500/login/fb/cb",
      profileFields: ["email", "gender", "locale", "displayName", "photos"],
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const user = await accountfbdb.findOne({ name: profile._json.name });
        if (user) {
          return done(null, user);
        }
        const newUser = new accountfbdb({
          id: profile._json.id,
          name: profile._json.name,
          email: profile._json.email,
          img:
            "https://graph.facebook.com/" +
            profile.id +
            "/picture" +
            "?width=200&height=200" +
            "&access_token=" +
            accessToken,
          provider: profile.provider,
          date_created: new Date(),
        });
        await newUser.save();
        return done(null, newUser);
      } catch (err) {
        return done(err);
      }
    }
  )
);

// Middleware kiểm tra previous path để xử lý
app.use((req, res, next) => {
  const acceptHeader = req.headers.accept || "";
  if (
    acceptHeader.includes("text/html") &&
    (req.path === "/" ||
      req.path === "/test" ||
      req.path === "/university" ||
      req.path === "/history/test")
  ) {
    req.session.currentPath = req.path;
  } else {
    req.session.currentPath = "/";
  }
  next();
});

// Xác thực tài khoản gg hoặc fb
passport.serializeUser((user, done) => {
  done(null, { name: user.name, provider: user.provider });
});

// Xác thực lại tài khoản gg hoặc fb
passport.deserializeUser(async ({ name, provider }, done) => {
  try {
    let user;
    if (provider === "google") {
      user = await accountggdb.findOne({ name });
    } else if (provider === "facebook") {
      user = await accountfbdb.findOne({ name });
    }
    done(null, user);
  } catch (err) {
    done(err);
  }
});

// Kết nối db
db.connect();

// Chạy WebServer
route(app);
httpServer.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
